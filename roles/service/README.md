# service
A role for registering system services.

## Requirements

### Packages
* Ansible v. 2.7.10

## Role Actions
* [`register`](#action-register)

## Role API

### Action `register`
Registers provided service in systemd, reloads the systemd daemon and launches the service.

#### Variables
* `service_name` - name of registered service
* *(optional)* `service_type` - [type](https://www.freedesktop.org/software/systemd/man/systemd.service.html#Options) of the service (default: `"simple"`) 
* *(optional)* `service_description` - description of a service (default:
    `"{{ service_name }} service"`)
* *(optional)* `service_requirements` - list of other services that the
    registered service requires to run. For more details see the `Requires=`
    section of `systemd.unit`  documentation.
* *(optional)* `service_order_after` - list of other services that should be
    started before the registered service. For more details see the `After=`
    section of `systemd.unit` documentation.
* *(optional)* `service_part_of` - list of other services that should propagate stop/restart actions to
    the registered service. For more details see the `PartOf=` section of `systemd.unit` documentation.
* *(optional)* `service_restart_policy` - service restart policy. For possible
    values and description see `Restart=` section of `systemd.service`
    documentation (default value: `always`).
* *(optional)* `service_wait_timeout` - time to wait for a service to
    start/stop. For more info see the `TimeoutSec=` section of `systemd.service`
    documentation (default value: `300`).
* *(optional*) `service_environment_file` - path to a file with the environment
    variables definition. This variables will be set for executed processes.
* *(optional)* `service_environment_variables` - dictionary of environmental variables.
    ```yaml
    service_environment_variables:
      VARIABLE1: "value1"
      VARIABLE2: "value2"
    ```
    :warning: **WARNING**: :warning: Variables defined in `service_environment_file` take precedence over those from  `service_environment_variables`.  
* `service_start_command` - command with its arguments that is executed when
    service is started. See the `ExecStart=` section of the `systemd.service`
    documentation for more info.
* `service_stop_command` - command with its arguments that is executed when
    service is stopped. See the `ExecStop=` section of the `systemd.service`
    documentation for more info.
* *(optional)* `service_stop_command_can_fail` - is the service stop command
    allowed to fail without consequences (default: `no`).
* *(optional)* `service_pre_start_commands` - List of commands that will be
    executed before `service_start_command` command. By default, if any of commands
    specified fails, service will not be started. To overwrite this behaviour,
    command must be specified as a dictionary with a property `command` with
    command and and property `can_fail` set to `yes`. Example:
    ```yaml
    service_pre_start_commands:
    - "echo \"This command must succeed\""
    - command: "echo \"This command must succeed too\""
        can_fail: no
    - command: "echo \"This command can fail\""
        can_fail: yes
    ```
For more details see the `ExecStartPre` section of the `systemd.service`
documentation.
* *(optional)* `service_additional_options` - additional options to be set in
    a service definition file. This must be a dictionary with a three optional
    subdictionaries. The keys are: `unit`, `service` and `install`. Each
    subdictionary corresponds to a section in a service definition file. Example:
    ```yaml
    service_additional_options:
      unit:
        IgnoreOnIsolate: "true"
      service:
        ExecReload: "echo 'reload'"
        FileDescriptorStoreMax: 7
    ```
* *(optional)* `service_force_restart` - force service restart independently of its configuration (default: `no`)

#### Declaring service environmental variables
There are two options to declare service environmental variables in `register` action:
* to declare more variables (by reading from file) use:
    ```yaml
    service_environment_file: "/full/path/to/my/env/file"
    ```
* to declare more variables by defining them in Ansible dictionary use:
    ```yaml
    service_environment_variables:
      VARIABLE1: "value1"
      VARIABLE2: "value2"
    ```
    
## License
[MIT](LICENSE.md)

## Author Information
Academic Computer Centre in Gdansk (CI TASK)

[task.gda.pl](https://task.gda.pl)

## Additional Resources
* [systemd.unit documentation](https://www.freedesktop.org/software/systemd/man/systemd.unit.html)
* [systemd.service documentation](https://www.freedesktop.org/software/systemd/man/systemd.service.html)
