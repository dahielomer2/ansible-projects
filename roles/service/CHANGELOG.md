# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]
## [1.5.2] - 2019-05-17
### Fixed
* Formatting inconsistency between role's versions. (#22)

## [1.5.1] - 2019-04-24
### Changed
* Ansible supported version to 2.7.10. (#20)
* *pipeline-worker* image version updated to 19.04. (#20)

### Fixed
* Template formatting when setting service dependencies. (#20)

## [1.5.0] - 2019-04-02
### Added
* Support for defining environment variables without using environment file. (#18)
* Ability to specify service type. (#18)

### Changed
* Ansible supported version to 2.7.9. (#18)
* *pipeline-worker* image version updated to 19.03. (#18)

## [1.4.0] - 2018-11-28
### Changed
* Ansible supported version to 2.6.3. (#15)
* Upgrade `README.md` to new requirements. (#15)
* Updated test to new requirements. (#15)

## [1.3.1] - 2018-04-24
### Changed
* Version of ansible to 2.5.0. (#13)
* *pipeline-worker* image version updated to 18.04. (#13)

## [1.3.0] - 2018-02-16
### Added
* Parameter `service_force_restart`. (#11)

## [1.2.0] - 2017-11-23
### Fixed
* Deprecation warning in ansible 2.4.0. (#9)

## [1.1.0] - 2017-07-06
### Changed
* Support for `PartOf` dependencies. (#7)
* Revert service template rename. (#5)

## [1.0.2] - 2017-05-16
### Fixed
* Rename service template. (#4)

## [1.0.1] - 2017-04-04
### Fixed
* Service start/restart when configuration has not been changed. (#2)

## 1.0.0 - 2017-01-11
### Changed
* Initial release.

[Unreleased]: https://projects.task.gda.pl/ansible-roles/service/compare/1.5.2...master
[1.5.2]: https://projects.task.gda.pl/ansible-roles/service/compare/1.5.1...1.5.2
[1.5.1]: https://projects.task.gda.pl/ansible-roles/service/compare/1.5.0...1.5.1
[1.5.0]: https://projects.task.gda.pl/ansible-roles/service/compare/1.4.0...1.5.0
[1.4.0]: https://projects.task.gda.pl/ansible-roles/service/compare/1.3.1...1.4.0
[1.3.1]: https://projects.task.gda.pl/ansible-roles/service/compare/1.3.0...1.3.1
[1.3.0]: https://projects.task.gda.pl/ansible-roles/service/compare/1.2.0...1.3.0
[1.2.0]: https://projects.task.gda.pl/ansible-roles/service/compare/1.1.0...1.2.0
[1.1.0]: https://projects.task.gda.pl/ansible-roles/service/compare/1.0.2...1.1.0
[1.0.2]: https://projects.task.gda.pl/ansible-roles/service/compare/1.0.1...1.0.2
[1.0.1]: https://projects.task.gda.pl/ansible-roles/service/compare/1.0.0...1.0.1
