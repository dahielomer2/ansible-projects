#!/usr/bin/env bash

function test_missing_parameters {
  EXTRA_VARS=$1
  RESULT_EXPECTED=$2
  echo "testing parameters: ${EXTRA_VARS}"

  ansible-playbook \
    --inventory-file inventory \
    --extra-vars "${EXTRA_VARS}" \
    test_lint_api.yml \
    | grep --quiet \
    "Parameters 'service_name', 'service_start_command' and 'service_stop_command' must be set to use this role."

  RESULT_ACTUAL=$?
  if (( $RESULT_ACTUAL != $RESULT_EXPECTED )); then
    echo "test failed with result: ${RESULT_ACTUAL} (expected: ${RESULT_EXPECTED})"
    exit -1
  fi
}

test_missing_parameters "service_name=test service_start_command=test" 0
test_missing_parameters "service_name=test service_stop_command=test" 0
test_missing_parameters "service_start_command=test service_stop_command=test" 0
test_missing_parameters "service_name=test service_start_command=test service_stop_command=test" 1

exit 0
