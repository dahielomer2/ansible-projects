# Docker Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [4.1.0] - 2019-11-13
### Changed
* *python2/ansible* updated to 2.8.6. (#80)
* *docker/pipeline-worker* updated to 19.10. (#80)
* *docker-ce* updated to 19.03.4. (#80)
* *docker-ce-cli* updated to 19.03.4. (#80)
* `docker:dind` version in CI tests to latest. (#80)

## [4.0.2] - 2019-10-07
### Fixed
* Docker service port filters drop packets from containers in the same network. (#75)

## [4.0.1] - 2019-07-26
### Fixed
* CI test by specifying docker dind container version 18.09.8-dind. (#74)
* Possible deadlocks when setting iptables chains. (#74)

## [4.0.0] - 2019-07-11
### Added
* *Disable* and *enable* actions. (#71)
* Specific docker version "18.09.7". (#68)
* Specific docker-compose version "1.24.1". (#68)
* Specific containerd.io version "1.2.6-3". (#68)

### Changed
* *Ansible* dependency version to `2.8.1`. (#68)
* *pipeline-worker* CI image version to `19.04`. (#69)

### Fixed
* `install_base` action reinstalls docker on each run. (#66)

### Removed
* Support for selecting `docker_compose_version`. (#68)
* Support for selecting `docker_version`. (#68)
* `docker_mount_shared` option. (#68)

## [3.2.0] - 2019-04-24
### Changed
* *Ansible* dependency version to `2.7.10`. (#69)
* *pipeline-worker* CI image version to `19.04`. (#69)
* *Ansible* dependency version to `2.7.7`. (#65)
* *pipeline-worker* CI image version to `19.02-1`. (#65)

### Fixed
* Setting system service dependencies (including *docker.service*). (69)
* *test-docker-compose* should be done for clean OSes. (#45)

### Removed
* Support for docker engine. (#67)

## [3.1.1] - 2019-01-17
### Fixed
* Ansible lint failing. (#63)

## [3.1.0] - 2018-12-21
### Added
* Support for Docker 18.09.0. (#62)
* Support for setting additional options on Docker network creation. (#59)

### Changed
* Ansible supported version to 2.7.5. (#59)
* *pipeline-worker* CI container version to 18.11. (#59)

## [3.0.0] - 2018-10-29
### Added
* `docker_service_force_restart` configuration variable to the `service_base` action. (#56)

### Changed
* Ansible supported version to 2.7.1. (#56)
* *pipeline-worker* CI container version to 18.10. (#56)

## [2.6.1] - 2018-10-08
### Changed
* Ansible supported version to 2.7.0. (#54)

### Fixed
* `docker_service_port_filters` block outgoing connections. (#54)

## [2.6.0] - 2018-10-02
### Added
* Support for port filtering. (#52)
* Enabling IP forwarding on the host. (#51)
* Support for Ubuntu 18.04 LTS Bionic Beaver. (#48)

### Changed
* Ansible supported version to 2.6.3. (#50)
* pip supported version to 10.0.1. (#50)
* Upgrade `README.md` to new requirements. (#50)
* Upgrade test to new requirements. (#50)

### Removed
* Support for Ubuntu 16.04 LTS Xenial Xerus. (#48)
* Support for 17.10 Artful Aardvark. (#48)

## [2.5.2] - 2018-05-17
### Changed
* Docker Compose installed from GitHub instead of pip. (#47)

## [2.5.1] - 2018-04-24
### Fixed
* Readme is misleading about installing docker ce versions from docker-engine repo. (#42)
* Installing on centos copies /etc/yum.conf from host running ansible instead of remote host. (#43)
* Installing base without ansible_user specified. (#41)

### Changed
* Version of ansible to 2.5.0. (#26)
* *pipeline-worker* image version updated to 18.04. (#20)
* *ansible-playbook* image version updated to 18.03. (#20)

## [2.5.0] - 2018-01-17
### Added
* `docker_additional_configuration` support. (#37)
* Support for `docker_log_rotate`. (#35)
* Support for custom docker version. (#28)
* Support for custom docker compose version. (#28)
* Support for `docker_data_directory`. (#28)
* Support for `docker_insecure_registries`. (#28)
* Support for `docker_mount_shared`. (#28)

### Fixed
* Docker daemon not restating. (#39)
* Redundant closing bracket. (#38)
* `install_base` fails if a very old Docker version is already present. (#20)

## [2.4.2] - 2017-11-27
### Fixed
* Idempotency in docker-compose. (#33)

## [2.4.1] - 2017-11-27
### Fixed
* Compose does not work on centos. (#31)

## [2.4.0] - 2017-10-09

### Changed
* Changelog format updated. (#29)
* Compatibility with Ansible 2.4.0.0. (#29)
* Docker Compose installed with *pip*. (#29)

### Removed
* *docker-py* is no longer supported. (#29)

## [2.3.0] - 2017-07-06

### Changed
* Services installed by this role are now restarted by default when the *Docker* service is restarted. (#25)

## [2.2.0] - 2017-05-16

### Added
* Added new option - `docker_service_pre_start_commands` for a list of service pre start commands. (#21)

### Changed
* Changed `include` to `include_role` (SSH role task). (#15)
* Changed user name variable used in `install_base` action. (#22)

### Fixed
* Fixed propagating `service_pre_start_commands`. (#21)

## [2.1.0] - 2017-03-17

### Added
* Added action to create docker network `network/create`. (#17)
* Added parameter `docker_service_network_name` to connect docker service to existing network. (#17)

## [2.0.0] - 2017-03-03

### Changed
* Added ssh:disconnect role usage when user is added to `docker` group. (#14)

## [1.1.1] - 2017-03-02

### Added
* Support for CentOS 7. (#12)

### Removed
* Removed parameter `fd://` from docker.service template (caused bug on CentOS). (#12)

## [1.1.0] - 2017-02-01

### Added
* Support for different storage drivers (specifically, `overlay`). (#7)

## 1.0.0

### Added
* Initial release.

[Unreleased]: https://projects.task.gda.pl/ansible-roles/docker/compare/4.1.0...master
[4.1.0]: https://projects.task.gda.pl/ansible-roles/docker/compare/4.0.2...4.1.0
[4.0.2]: https://projects.task.gda.pl/ansible-roles/docker/compare/4.0.1...4.0.2
[4.0.1]: https://projects.task.gda.pl/ansible-roles/docker/compare/4.0.0...4.0.1
[4.0.0]: https://projects.task.gda.pl/ansible-roles/docker/compare/3.2.0...4.0.0
[3.2.0]: https://projects.task.gda.pl/ansible-roles/docker/compare/3.1.1...3.2.0
[3.1.1]: https://projects.task.gda.pl/ansible-roles/docker/compare/3.1.0...3.1.1
[3.1.0]: https://projects.task.gda.pl/ansible-roles/docker/compare/3.0.0...3.1.0
[3.0.0]: https://projects.task.gda.pl/ansible-roles/docker/compare/2.6.1...3.0.0
[2.6.1]: https://projects.task.gda.pl/ansible-roles/docker/compare/2.6.0...2.6.1
[2.6.0]: https://projects.task.gda.pl/ansible-roles/docker/compare/2.5.2...2.6.0
[2.5.2]: https://projects.task.gda.pl/ansible-roles/docker/compare/2.5.1...2.5.2
[2.5.1]: https://projects.task.gda.pl/ansible-roles/docker/compare/2.5.0...2.5.1
[2.5.0]: https://projects.task.gda.pl/ansible-roles/docker/compare/2.4.2...2.5.0
[2.4.2]: https://projects.task.gda.pl/ansible-roles/docker/compare/2.4.1...2.4.2
[2.4.1]: https://projects.task.gda.pl/ansible-roles/docker/compare/2.4.0...2.4.1
[2.4.0]: https://projects.task.gda.pl/ansible-roles/docker/compare/2.3.0...2.4.0
[2.3.0]: https://projects.task.gda.pl/ansible-roles/docker/compare/2.2.0...2.3.0
[2.2.0]: https://projects.task.gda.pl/ansible-roles/docker/compare/2.1.0...2.2.0
[2.1.0]: https://projects.task.gda.pl/ansible-roles/docker/compare/2.0.0...2.1.0
[2.0.0]: https://projects.task.gda.pl/ansible-roles/docker/compare/1.1.1...2.0.0
[1.1.1]: https://projects.task.gda.pl/ansible-roles/docker/compare/1.1.0...1.1.1
[1.1.0]: https://projects.task.gda.pl/ansible-roles/docker/compare/1.0.0...1.1.0
