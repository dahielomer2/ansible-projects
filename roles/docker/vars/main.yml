---
#User-defined Docker Compose service options
_docker_compose_executable: "/usr/local/bin/docker-compose"
_docker_compose_service_command: "{{ _docker_compose_executable }} --file \"{{ docker_compose_service_file }}\" --project-name \"{{ docker_compose_service_project_name }}\""
_docker_compose_service_pre_start_commands:
  - command: "{{ _docker_compose_service_command }} down"
    can_fail: yes
  - "{{ _docker_compose_service_command }} pull"
_docker_compose_service_start_command: "{{ _docker_compose_service_command }} up"
_docker_compose_service_stop_command: "{{ _docker_compose_service_command }} down"

#User-defined Docker service options
_docker_service_start_command: >-
  /usr/bin/docker run \
      --name {{ docker_service_container_name }} \
  {% for _port in docker_service_ports | default([]) %}
      --publish {{ _port }} \
  {% endfor %}
  {%- if docker_service_network_name is defined %}
      --network={{ docker_service_network_name }} \
  {% endif %}
  {%- for _volume in docker_service_volumes | default([]) %}
      --volume {{ _volume }} \
  {% endfor %}
  {%- for _key,_value in (docker_service_environment_variables | default({})).iteritems() %}
      --env {{ _key }}={{ _value }} \
  {% endfor %}
  {%- if docker_service_environment_file is defined %}
      --env-file={{ docker_service_environment_file }} \
  {% endif %}
  {%- if docker_service_parameters is defined %}
      {{ docker_service_parameters }} \
  {% endif %}
      {{ docker_service_image }} {{ docker_service_command | default() }}
_docker_service_pre_start_commands:
  - command: "/usr/bin/docker rm {{ docker_service_container_name }}"
    can_fail: yes
  - "/usr/bin/docker pull {{ docker_service_image }}"
_docker_service_stop_command: "/usr/bin/docker stop {{ docker_service_container_name }}"
_docker_service_scripts_directory: "/usr/local/bin/docker.services"
_docker_service_requirements: "{{ ['docker.service'] + docker_service_requirements }}"
_docker_service_order_after: "{{ ['docker.service'] + docker_service_order_after }}"
_docker_service_part_of: "{{ ['docker.service'] + docker_service_part_of }}"

_docker_packages:
  centos:
    - "docker-ce-19.03.4"
    - "docker-ce-cli-19.03.4"
    - "containerd.io-1.2.6"
  ubuntu:
    - "docker-ce=5:19.03.4~3-0~ubuntu-{{ ansible_lsb.codename | default() }}"
    - "docker-ce-cli=5:19.03.4~3-0~ubuntu-{{ ansible_lsb.codename | default() }}"
    - "containerd.io=1.2.6-3"
_docker_compose_version: "1.24.1"

_docker_configuration: |
  {
    "data-root": "{{ docker_data_directory }}",

    {% if docker_storage_driver | default() %}
      "storage-driver": "{{ docker_storage_driver }}",
    {% endif %}

    "mtu": {{ docker_container_mtu }},

    {% if docker_log_rotate | default() %}
      "log-driver": "json-file",
      "log-opts": {
        "max-size": "{{ docker_log_rotate_size }}",
        "max-file": "{{ docker_log_rotate_count }}"
      },
    {% endif %}

    "insecure-registries": {{ docker_insecure_registries | to_json }}
  }
