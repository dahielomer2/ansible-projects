# Update procedure

## From 2.6.1
### `service_base` action
Refactor your calls to the `service_base` action to use `docker_service_force_restart` instead of `service_force_restart`. 

## From 2.6.0
* :warning: Warning :warning: - Since `ansible` is updated to `2.7.0` templates will be generated differently, which may cause restart of docker and all containers unintentionally.

* If you were using `docker_service_port_filters`, after update you should run:
    ```bash
    iptables --delete DOCKER-USER --match mark --mark 0x7 -j DROP
    ```
