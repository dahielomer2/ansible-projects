# docker
A role for managing your Docker installation.

## Requirements

### Packages
* Ansible v. 2.8.6

### Roles
* [service](https://projects.task.gda.pl/ansible-roles/service) - this role is used by the
    `docker:service/*` actions.
* [ssh](https://projects.task.gda.pl/ansible-roles/ssh) - this role is used by the `install_base`
    action to terminate ssh connections to enforce refreshing linux user groups

### Managed hosts configuration
* [pip](https://pypi.python.org/pypi/pip) v. 10.0.1

## Role Actions
* [`disable`](#action-disable)
* [`enable`](#action-enable)
* [`install_base`](#action-install_base)
* [`install_compose`](#action-install_compose)
* [`service_base`](#action-service_base)
* [`service_compose`](#action-service_compose)
* [`network/create`](#action-networkcreate)

## Role API

### Action `disable`
Stops and disables docker service and shuts down all active containers.

### Action `enable`
Starts and enables docker service. Please note that all containers with restart policy set to *always* or
*unless-stopped* will start automatically.

### Action `install_base`
Installs/updates Docker (version *19.03.4*).

#### Variables
* *(optional)* `docker_container_mtu` - requested maximum transmission unit (MTU) for Docker containers (default: `1450`)
* *(optional)* `docker_storage_driver` - force the Docker runtime to use a specific storage driver.
* *(optional)* `docker_additional_configuration` - custom [docker settings](https://docs.docker.com/engine/reference/commandline/dockerd/#on-linux)
    (default: `{}`)
* *(optional)* `docker_data_directory` - docker root data directory (default: `"/var/lib/docker/"`)
* *(optional)* `docker_insecure_registries` - list of insecure registries to be added (default: `[]`)
* *(optional)* `docker_log_rotate` - enables docker logs rotating using json-file (default: `no`)
* *(optional)* `docker_log_rotate_size` - size limit for single log file when `docker_log_rotate` is enabled (default: `"10m"`)
* *(optional)* `docker_log_rotate_count` - number of log files to keep when `docker_log_rotate` is enabled (default: `"3"`)

### Action `install_compose`
Installs Docker Compose (version *1.24.1*).

### Action `service_base`
Registers user-defined `docker` service in `systemd`.

#### Variables
* `service_name` - name of registered service
* *(optional)* `service_description` - description of a service (default:
    `"{{ service_name }} service"`)
* *(optional)* `docker_service_pre_start_commands` - list of pre-tasks for a service. Please refer to
    [service role](https://projects.task.gda.pl/ansible-roles/service) for more information about the format.
* `docker_service_image` - docker image name to execute
* *(optional)* `docker_service_container_name` - name of a container (default: "`{{ service_name }}"`)
* *(optional)* `docker_service_volumes` - list of volumes mounted to a container
* *(optional)* `docker_service_environment_file` - path to a file with environment variables definition
* *(optional)* `docker_service_environment_variables` - a dictionary of environment variables to be
    set in container (e.g. `{ DEBUG: "True" }`)
* *(optional)* `docker_service_ports` - list of container's ports published to a host.
    Format should be the same as in standard `docker run` command (e.g. `["80:80"]`)
    :warning: Warning :warning: - by default access to all ports published by docker will not be filtered by any standard means,
    because the way docker manages firewall rules cause all standard firewall rule sets to be bypassed.
    If you want to limit access to those ports use `docker_service_port_filters`.
* *(optional)* `docker_service_port_filters` - list of dictionaries describing filters for ports (default: not set):
    :warning: Warning :warning: - service name will be limited to 25 characters
    :warning: Warning :warning: - filtering per interface is not supported
    :warning: Warning :warning: - if you want to have access from other containers, you need to allow input-output on host with source/destination of docker bridges.
    * `external_port` - external port that will be filtered
    * *(optional)* `protocol` - port protocol (one of: `"tcp"`, `"udp"`, default: `"tcp"`)
    * `allowed_subnets` - list of subnets that will have access to filtered port
* *(optional)* `docker_service_network_name` - name of the docker network where container will be connected to
* *(optional)* `docker_service_parameters` - additional docker parameters (e.g. `"--privileged"`)
* *(optional)* `docker_service_command` - command tu execute in container (e.g. `"sleep --forever --and-even-more=7"`)
* *(optional)* `docker_service_force_restart` - should the service be restarted after it's been configured (default: `no`)
    :warning: Warning :warning: - do not use `service_force_restart` for this purpose!
* *(optional)* `docker_service_requirements` - list of other services that the
    registered service requires to run. For more details see the `Requires=`
    section of `systemd.unit`  documentation
    (default: `[]`, `docker.service` is added to the list automatically).
* *(optional)* `docker_service_order_after` - list of other services that should be
    started before the registered service. For more details see the `After=`
    section of `systemd.unit` documentation
    (default: `[]`, `docker.service` is added to the list automatically).
* *(optional)* `docker_service_part_of` - list of other services that should propagate stop/restart actions to
    the registered service. For more details see the `PartOf=` section of `systemd.unit` documentation
    (default: `[]`, `docker.service` is added to the list automatically).

### Action `service_compose`
Registers user-defined `docker-compose` service in `systemd`.

#### Variables
* `service_name` - name of registered service
* *(optional)* `service_description` - description of a service (default: `"{{ service_name }} service"`)
* *(optional)* `docker_service_pre_start_commands` - list of pre-tasks for a service. Please refer to
    [service role](https://projects.task.gda.pl/ansible-roles/service) for more information about the format.
* *(optional)* `docker_service_requirements` - list of other services that the
    registered service requires to run. For more details see the `Requires=`
    section of `systemd.unit`  documentation
    (default: `[]`, `docker.service` is added to the list automatically).
* *(optional)* `docker_service_order_after` - list of other services that should be
    started before the registered service. For more details see the `After=`
    section of `systemd.unit` documentation
    (default: `[]`, `docker.service` is added to the list automatically).
* *(optional)* `docker_service_part_of` - list of other services that should propagate stop/restart actions to
    the registered service. For more details see the `PartOf=` section of `systemd.unit` documentation
    (default: `[]`, `docker.service` is added to the list automatically).
* `docker_compose_service_file` - path to a file with a docker compose definition.
    This file should exist before role execution
* *(optional)* `docker_compose_service_project_name` - Docker Compose project name (default: `"{{ service_name }}"`)

### Action `network/create`
Creates docker network with driver `bridge`.

#### Variables
* `docker_network_name` - name of a network to create
* *(optional)* `docker_network_options` - dictionary with additional options for the Docker bridge driver, as described [here](https://docs.docker.com/engine/reference/commandline/network_create/#bridge-driver-options)

    Default value:
    ```yml
    docker_network_options:
      "com.docker.network.driver.mtu": "{{ docker_container_mtu }}"
    ```

## Additional Information

### Further configuration of user-defined service
Moreover, user can redefine any of the `service` role variables. For the full list of available options
you should visit the `service` role project page.

**Achtung! Attention! Attentie! Attention! Atención! Attenzione!**
Modifying variables listed below is not recommended. Most of them use other variables described above
so it should be done with caution. Probably you should check the exact variable definition
in a source code of this role.
* `service_start_command` - `docker-compose up ...` or `docker run ...`
* `service_stop_command` - `docker-compose down ...` or `docker stop ...`
* `docker_service_pre_start_commands` -
    * for Docker Compose:

    ```yaml
    - command: "docker-compose down ..."
      can_fail: yes
    - "docker-compose pull ..."
    ```

    * for Docker:

    ```yaml
    - command: "docker rm ..."
      can_fail: yes
    - "docker pull"
    ```

## License
[MIT](LICENSE.md)

## Author Information
[Centre of Informatics - Tricity Academic Supercomputer & networK (CI TASK)](https://task.gda.pl/home-en)


## Update
Update may require additional steps, refer to [UPDATE.md](UPDATE.md) for more details.
