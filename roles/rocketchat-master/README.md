# Rocket.Chat
A role for deploying Rocket.Chat

## Versions
* *python2/ansible* 2.9.4

## External ansible roles
* [`docker`](https://projects.task.gda.pl/ansible-roles/docker)
* [`service`](https://projects.task.gda.pl/ansible-roles/service)

## Managed hosts configuration
* `docker`
* `docker-compose`

## Role Actions
* [`deploy`](#action-deploy)

## Role API
### Action `deploy`
Deploys Rocket.Chat on a host.

#### Variables
##### General Rocket.Chat options
* *(optional)* `rocketchat_enable_https` - is rocketchat behind HTTPS proxy (default: `no`)
* `rocketchat_fqdn` - FQDN of Rocket.Chat
* *(optional)* `rocketchat_mtu` - network maximum transmission unit for Rocket.Chat (default: `1450`)
* *(optional)* `rocketchat_port` - port of Rocket.Chat service (default: `80`)
* *(optional)* `rocketchat_service_name` - systemd service name for Rocket.Chat (default:
    `"docker-rocketchat"`)
* `rocketchat_data_path` - absolute path where Rocket.Chat and other services will store their data

##### Rocket.Chat admin configuration
You can specify admin user credentials. You should specify either all or none of the below options.
If you won't specify any, the admin user will not be created. This options are valid only when
deploying new server, setting them on the existing Rocket.Chat server does not change anything.
* `rocketchat_admin_user` - admin user name
* `rocketchat_admin_password` - admin user password
* `rocketchat_admin_mail` - admin user email address

### RocketBot options
Options not marked as *optional* are needed ONLY if `rocketchat_use_bot` is set to `yes`.
* *(optional)* `rocketchat_use_bot` - should bot be installed (default: `yes`)
* *(optional)* `rocketchat_bot_room` - room name, where bot should be active (default: `""`)
* *(optional)* `rocketchat_bot_listen_on_all_public` - should bot be active in all public rooms. If
    set to `yes`, this overrides `rocketchat_bot_room` option (default: `yes`)
* *(optional)* `rocketchat_bot_name` - bot user name. If the user does not exist and
    `rocketchat_admin_user` and `rocketchat_admin_password` variables are set, it will be created.
    If it already exists, it will be updated. (default: `"rocket.cat"`)
* `rocketchat_bot_password` - password of a bot user
* *(optional)* `rocketchat_bot_mail` - email address of a bot user (default: `"bot@{{ rocketchat_fqdn }}"`)
* *(optional)* `rocketchat_bot_display_name` - bot user display name (default: `"Rocket.Cat"`)
* *(optional)* `rocketchat_bot_external_scripts` - a list of external hubot scripts that should be
    used by hubot. List of available scripts is available
    [here](https://hubot-script-catalog.herokuapp.com/). (default: `["hubot-help", "hubot-9gag",
    "hubot-business-cat", "hubot-motivate", "hubot-dogeme", "hubot-soon", "hubot-devopsreactions",
    "hubot-doge", "hubot-giphy-gifme", "hubot-glados-taunts", "hubot-xkcd,hubot-diagnostics"]`)
* *(optional)* `rocketchat_bot_custom_scripts_directory` - a path to user own hubot scripts that
    should be copied to and used by rocketbot. It can be absolute or relative. If path is a directory,
    it is copied recursively. In this case, path shold ends with "/".
* *(optional)* `rocketchat_bot_environment_variables` - a dictionary of custom environment
    variables to be added to rocketbot. It can be useful when your hubot script requires any additional
    configuration (default: `{}`).

## Update:
Update may require additional steps, refer to [UPDATE.md](UPDATE.md) for more details.

## License
[MIT](LICENSE.md)

## Author Information
[Centre of Informatics - Tricity Academic Supercomputer & networK (CI TASK)](https://task.gda.pl/home-en)

## Additional Resources
* [docker role](https://projects.task.gda.pl/ansible-roles/docker)
* [Rocket.Chat](https://github.com/RocketChat/Rocket.Chat)
* [hubot scripts catalog](https://hubot-script-catalog.herokuapp.com/)
