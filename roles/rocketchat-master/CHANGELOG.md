# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [4.7.0] - 2020-02-24
### Changed
* License year to 2020. (#64)
* *ansible-roles/docker* updated to 4.1.0. (#64)
* *docker/mongo* updated to 4.0.16. (#64)
* *docker/rocketchat* updated to 3.0.2. (#64)
* *docker/pipeline-worker* updated to 19.12. (#64)
* *python2/ansible* updated to 2.9.4. (#64)

## [4.6.0] - 2019-10-22
### Changed
* *ansible-roles/docker* updated to 4.0.2. (#62)
* *docker/mongo* updated to 4.0.13. (#62)
* *docker/rocketchat* updated to 2.1.1. (#62)

## [4.5.0] - 2019-10-03
### Changed
* *docker/rocketchat* updated to 2.1.0. (#60)
* *docker/pipeline-worker* updated to 19.09. (#60)
* *python2/ansible* updated to 2.8.5. (#60)

## [4.4.0] - 2019-09-16
### Changed
* Code adapted to new standards. (#58)
* *docker/rocketchat* updated to 2.0.0. (#58)
* *docker/pipeline-worker* updated to 19.08-1. (#58)

## [4.3.0] - 2019-08-21
### Added
* Autoupdater check configuration in CI job. (#56)

### Changed
* *docker/mongo* updated to 4.0.12. (#56)
* *docker/rocketchat* updated to 1.3.2. (#56)
* *python2/ansible* updated to 2.8.4. (#56)
* *docker/pipeline-worker* updated to 19.08. (#56)

## [4.2.0] - 2019-07-31
### Changed
* *ansible-roles/docker* updated to 4.0.1. (#53)
* *docker/mongo* updated to 4.0.11. (#53)
* *docker/pipeline-worker* updated to 19.07-1. (#53)
* *docker/rocketchat* updated to 1.2.3. (#53)
* *python2/ansible* updated to 2.8.3. (#53)

## [4.1.0] - 2019-07-10
### Added
* File for autoupdater. (#16)
* `tests/galaxy-requirements.yml` file with external ansible role dependencies. (#16)
* `requirements.txt` with python2 package dependencies. (#16)

### Changed
* *docker/mongo* updated to 4.0.10. (#52)
* *docker/rocketchat* updated to 1.2.1. (#52)
* *docker/pipeline-worker* updated to 19.07. (#52)
* *python2/ansible* updated to 2.8.2. (#52)

## [4.0.0] - 2019-05-13
### Changed
* *Rocket.Chat* version to `1.0.3`. (#46)
* *mongo* version to `4.0.9`. (#46)

## [3.0.2] - 2019-04-24
### Changed
* *ansible* updated to 2.7.10. (#48)
* *pipeline-worker* updated to 19.04. (#48)

### Fixed
* Role fails to login after service start. (#48)

## [3.0.1] - 2019-02-14
### Changed
* *Rocket.Chat* version to `0.74.3`. (#46)
* *Ansible* supported version to `2.7.7`. (#46)
* *pipeline-worker* image version to `19.02-1`. (#46)

## [3.0.0] - 2019-02-07
### Changed
* *Rocket.Chat* updated to `0.74.2`. (#43)

### Removed
* `backup/create` action. (#43)
* `backup/restore` action. (#43)

## [2.4.1] - 2019-02-04
### Changed
* Rocket.Chat updated to 0.74.1. (#41)

## [2.4.0] - 2019-02-01
### Changed
* Rocket.Chat updated to 0.74.0. (#39)

## [2.3.0] - 2019-01-29
### Changed
* Rocket.Chat updated to 0.73.2. (#37)
* MongoDB downgraded to 3.6.10. (#37)
* Ansible updated to 2.7.6. (#37)
* Test container updated to *pipeline-worker* 19.01-1. (#37)

## [2.2.0] - 2018-08-28
### Changed
* `Rocketchat` updated to `0.69.0`. (#34)

### Removed
* Workaroud for user list API [bug](https://github.com/RocketChat/Rocket.Chat/issues/11599). (#34)

## [2.1.0] - 2018-08-20
### Added
* Workaroud for user list API [bug](https://github.com/RocketChat/Rocket.Chat/issues/11599). (#32)

### Changed
* `Rocketchat` updated to `0.68.4`. (#32)
* Ansible supported version to `2.6.3`. (#32)
* `pipeline-worker` to `18.08`. (#32)

## [2.0.0] - 2018-06-20
### Changed
* Upgrade `README.md` to new requirements. (#29)
* Ansible supported version to 2.5.5. (#29)
* Rocketchat updated to 0.65.2. (#30)
* Ansible supported version to 2.5.4. (#28)
* Rocketchat updated to 0.65.1. (#28)
* Mongo upgraded to 3.7.9. (#28)

## [1.5.0] - 2018-04-04
### Changed
* Ansible supported version to 2.5.0. (#26)
* Rocketchat updated to 0.63.0. (#26)

## [1.4.2] - 2018-03-26
### Changed
* Mongo upgraded to 3.6.3. (#24)
* Rocketchat updated to 0.62.2. (#24)

## [1.4.1] - 2018-03-12
### Changed
* Mongo upgraded to 3.4.13. (#22)
* Rocketchat updated to 0.62.1. (#22)

## [1.4.0] - 2017-11-23
### Added
* Support for mtu in compose. (#20)

### Changed
* Hubot rocketchat upgraded to 1.0.10. (#19)
* Rocketchat updated to 0.59.3. (#18)

### Fixed
* Compatibility issue with ansible 2.4.0.0. (#17)

## [1.3.0] - 2017-07-03
### Changed
* Hubot rocketchat upgraded to 1.0.8. (#14)
* Mongodb updated to 3.2.14. (#12)
* Rocketchat updated to 0.56.0. (#13)

## [1.2.0] - 2017-04-03
### Added
* Added `rocketchat_bot_environment_variables` to specify custom environment variables for bot. (#10)

### Fixed
* Fixed backup restore - database had to be empty before restoring. (#9)

## [1.1.0] - 2017-03-15
### Added
* Added `backup/*` actions. (#5)
* Automated bot user creation/modification. (#3)

### Fixed
* Fixed bot user update problem when bot has no emails defined. (#7)
* Fixed chown problem on database directory. (#4)

### Removed
* Removed `hubot-businesscat` script - the cause of rocketbot container failure. (#2)

## 1.0.0 - 2017-02-22
### Added
* Initial version of `rocketchat` role. (#1)

[Unreleased]: https://projects.task.gda.pl/ansible-roles/rocketchat/compare/4.7.0...master
[4.7.0]: https://projects.task.gda.pl/ansible-roles/rocketchat/compare/4.6.0...4.7.0
[4.6.0]: https://projects.task.gda.pl/ansible-roles/rocketchat/compare/4.5.0...4.6.0
[4.5.0]: https://projects.task.gda.pl/ansible-roles/rocketchat/compare/4.4.0...4.5.0
[4.4.0]: https://projects.task.gda.pl/ansible-roles/rocketchat/compare/4.3.0...4.4.0
[4.3.0]: https://projects.task.gda.pl/ansible-roles/rocketchat/compare/4.2.0...4.3.0
[4.2.0]: https://projects.task.gda.pl/ansible-roles/rocketchat/compare/4.1.0...4.2.0
[4.1.0]: https://projects.task.gda.pl/ansible-roles/rocketchat/compare/4.0.0...4.1.0
[4.0.0]: https://projects.task.gda.pl/ansible-roles/rocketchat/compare/3.0.2...4.0.0
[3.0.2]: https://projects.task.gda.pl/ansible-roles/rocketchat/compare/3.0.1...3.0.2
[3.0.1]: https://projects.task.gda.pl/ansible-roles/rocketchat/compare/3.0.0...3.0.1
[3.0.0]: https://projects.task.gda.pl/ansible-roles/rocketchat/compare/2.4.1...3.0.0
[2.4.1]: https://projects.task.gda.pl/ansible-roles/rocketchat/compare/2.4.0...2.4.1
[2.4.0]: https://projects.task.gda.pl/ansible-roles/rocketchat/compare/2.3.0...2.4.0
[2.3.0]: https://projects.task.gda.pl/ansible-roles/rocketchat/compare/2.2.0...2.3.0
[2.2.0]: https://projects.task.gda.pl/ansible-roles/rocketchat/compare/2.1.0...2.2.0
[2.1.0]: https://projects.task.gda.pl/ansible-roles/rocketchat/compare/2.0.0...2.1.0
[2.0.0]: https://projects.task.gda.pl/ansible-roles/rocketchat/compare/1.5.0...2.0.0
[1.5.0]: https://projects.task.gda.pl/ansible-roles/rocketchat/compare/1.4.2...1.5.0
[1.4.2]: https://projects.task.gda.pl/ansible-roles/rocketchat/compare/1.4.1...1.4.2
[1.4.1]: https://projects.task.gda.pl/ansible-roles/rocketchat/compare/1.4.0...1.4.1
[1.4.0]: https://projects.task.gda.pl/ansible-roles/rocketchat/compare/1.3.0...1.4.0
[1.3.0]: https://projects.task.gda.pl/ansible-roles/rocketchat/compare/1.2.0...1.3.0
[1.2.0]: https://projects.task.gda.pl/ansible-roles/rocketchat/compare/1.1.0...1.2.0
[1.1.0]: https://projects.task.gda.pl/ansible-roles/rocketchat/compare/1.0.0...1.1.0
