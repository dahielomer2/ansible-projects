# Update procedure
Update should be always done to next tagged version (never jump more then 1 version at a time).

## From 1.5.0
* Before upgrade ensure mongo `featureCompatibilityVersion` is `3.6`
    ```
    docker exec rocketchat-database mongo --eval "db.adminCommand({ getParameter: 1, featureCompatibilityVersion: 1 })"
    ```
    if needed change using following command
    ```
    docker exec rocketchat-database mongo --eval 'db.adminCommand( { setFeatureCompatibilityVersion: "3.6" } )'
    ```

## From 1.4.1
* Before upgrade ensure mongo `featureCompatibilityVersion` is `3.4`

    ```
    docker exec rocketchat-database mongo --eval "db.adminCommand({ getParameter: 1, featureCompatibilityVersion: 1 })"
    ```
    if needed change using following command

    ```
    docker exec rocketchat-database mongo --eval 'db.adminCommand( { setFeatureCompatibilityVersion: "3.4" } )'
    ```
